import java.util.Scanner;

public class XOPattern {

	static String ty, ty2;
	
	public static void main(String[] args) {
		
			Scanner keyboard = new Scanner(System.in);
			System.out.print("How wide should the pattern be? ");
		int wide = keyboard.nextInt();
			System.out.print("How tall should the pattern be? ");
		int tall = keyboard.nextInt();
			keyboard.close();
			System.out.println(" ");

		if ((wide + tall) % 2 == 0) { //I added this "switch" statement because I didn't understand
			ty = "X";				  //how it showed "X" in your example since the first total will be odd
			ty2 = "O";				  //for example 5 wide and 8 tall = 13 so should print "O" but in yours it's "X"
		} else {
			ty = "O";
			ty2 = "X";
		}
		
		System.out.print("+");
		for (int j = wide; j > 0; j -= 1) {
			System.out.print("-");
		}
		System.out.print("+");
		System.out.println();
		for (int i = tall; i > 0; i -= 1) {
			System.out.print("|");
			for (int j = wide; j > 0; j -= 1) {
				if ((i + j) % 2 == 0) {
					System.out.print(ty);
				} else {
					System.out.print(ty2);
				}
				
			}
			System.out.print("|");
			System.out.println();
		}
		System.out.print("+");
		for (int j = wide; j > 0; j -= 1) {
			System.out.print("-");
		}
		System.out.print("+");
	}
}
